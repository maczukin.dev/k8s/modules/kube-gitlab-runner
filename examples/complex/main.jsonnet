local runner = import 'kube-gitlab-runner/main.libsonnet';

local defaultSecrets = {
  registrationToken: error 'must provide `registrationToken`',
};

function(secrets={})
  local _secrets = defaultSecrets + secrets;

  assert std.isObject(_secrets) : '`_secrets` must be an object';

  runner({
    local s = self,

    namespace: 'complex',
    releaseName: 'complex',
    rbac+: {
      create: true,
      serviceAccountName: 'gitlab-runner-sa',
    },
    tokens+: {
      registration: _secrets.registrationToken,
    },
    registration+: {
      URL: 'https://gitlab.com',
      tagList: [
        'complex',
        'kube-gitlab-runner',
      ],
      maxRegisterAttempts: 3,
    },
    configTemplateTOML: |||
      [[runners]]
        [runners.kubernetes]
          namespace = "%(namespace)s"
          pull_policy = "if-not-present"
    ||| % {
      namespace: s.namespace,
    },
  })
