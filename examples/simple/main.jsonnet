local runner = import 'kube-gitlab-runner/main.libsonnet';

local defaultSecrets = {
  registrationToken: error 'must provide `registrationToken`',
};

function(secrets={})
  local _secrets = defaultSecrets + secrets;

  assert std.isObject(_secrets) : '`_secrets` must be an object';

  runner({
    namespace: 'simple',
    releaseName: 'simple',
    tokens+: {
      registration: _secrets.registrationToken,
    },
    registration+: {
      URL: 'https://gitlab.com',
      tagList: [
        'simple',
        'kube-gitlab-runner',
      ],
      maxRegisterAttempts: 3,
    },
  })
