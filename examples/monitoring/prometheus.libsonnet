local kubePrometheus = import 'github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus/main.libsonnet';

local defaultParams = {
  initializeOnly: error 'must provide `initializeOnly`',
  prometheus: {
    namespace: error 'must provide `prometheus.namespace`',
    additionalNamespaces: error 'must provide `prometheus.additionalNamespaces`',
  },
};


function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';
  assert std.isArray(config.prometheus.additionalNamespaces) : '`prometheus.additionalNamespaces` must be an array';

  local kp =
    kubePrometheus
    {
      values+:: {
        common+: {
          namespace: config.prometheus.namespace,
          versions+: {
            prometheusOperator: '0.50.0',
          },
        },
        prometheus+: {
          namespaces+: config.prometheus.additionalNamespaces,
        },
      },
    };

  {
    ['prometheus-operator-' + name]: kp.prometheusOperator[name]
    for name in std.filter((function(name) kp.prometheusOperator[name].kind == 'CustomResourceDefinition'), std.objectFields(kp.prometheusOperator))
  } +
  {
    'kube-prometheus-namespace': kp.kubePrometheus.namespace,
  } +
  if !config.initializeOnly then
    {
      ['prometheus-operator-' + name]: kp.prometheusOperator[name]
      for name in std.filter((function(name) kp.prometheusOperator[name].kind != 'CustomResourceDefinition'), std.objectFields(kp.prometheusOperator))
    } +
    {
      ['kube-prometheus-' + name]: kp.kubePrometheus[name]
      for name in std.filter((function(name) name != 'namespace'), std.objectFields(kp.kubePrometheus))
    } +
    {
      ['prometheus-' + name]: kp.prometheus[name]
      for name in std.objectFields(kp.prometheus)
    }
  else
    {}
