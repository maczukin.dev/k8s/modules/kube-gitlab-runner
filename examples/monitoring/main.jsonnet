local prometheus = import './prometheus.libsonnet';
local runner = import './runner.libsonnet';

local defaultParams = {
  initializeOnly: false,
};

function(secrets={})
  local config =
    defaultParams
    {
      secrets: secrets,
    };

  local runnerShardNamespace = function(shard)
    'runners-%s' % shard;

  local runnerShard = function(shard)
    {
      ['%s-runner' % shard]: runner(
        config {
          runner: {
            shard: shard,
            namespace: runnerShardNamespace(shard),
          },
        }
      ),
    };

  local runnerShards = [
    'experimental',
    'beta',
  ];

  [
    prometheus(
      config {
        prometheus: {
          namespace: 'monitoring',
          additionalNamespaces: [
            runnerShardNamespace(shard)
            for shard in runnerShards
          ],
        },
      }
    ),
  ]
  +
  [
    runnerShard(shard)
    for shard in runnerShards
  ]
