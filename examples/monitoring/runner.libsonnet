local runner = import 'kube-gitlab-runner/main.libsonnet';

local defaultParams = {
  initializeOnly: error 'must provide `initializeOnly`',
  secrets: {
    registrationToken: error 'must provide `registrationToken`',
  },
  runner: {
    namespace: error 'must provider `runner.namespace`',
    shard: error 'must provide `runner.shard`',
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  local r = runner({
    local s = self,

    namespace: config.runner.namespace,
    releaseName: config.runner.shard,
    labels: {
      shard: config.runner.shard,
    },
    rbac+: {
      create: true,
    },
    deployment+: {
      replicas: 2,
    },
    metrics+: {
      podMonitor: {
        podTargetLabels: ['shard'],
      },
    },
    tokens+: {
      registration: config.secrets.registrationToken,
    },
    registration+: {
      URL: 'https://gitlab.com',
      tagList: [
        'monitored',
        'kube-gitlab-runner',
      ],
      maxRegisterAttempts: 3,
    },
    configTemplateTOML: |||
      [[runners]]
        [runners.kubernetes]
          namespace = "%(namespace)s"
          pull_policy = "if-not-present"
    ||| % {
      namespace: s.namespace,
    },
  });

  { 'runner-namespace': r.namespace } +
  if !config.initializeOnly then
    {
      ['runner-' + name]: r[name]
      for name in std.objectFields(r)
    }

  else
    {}
