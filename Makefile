VERSION ?= $(shell git describe)

include Makefile.*.mk

.PHONY: build-gitlab-ci-yaml
build-gitlab-ci-yaml:
	@jsonnet \
		-S \
		--output-file .gitlab-ci.yml \
		.gitlab/ci/main.jsonnet

.PHONY: check-gitlab-ci-yml
check-gitlab-ci-yml:
	@$(MAKE) build-gitlab-ci-yaml
	@git --no-pager diff --compact-summary --exit-code -- .gitlab-ci.yml && \
		echo ".gitlab-ci.yml is up-to-date!"

.PHONY: dependencies-install
dependencies-install:
	@jb install

.PHONY: jsonnet-fmt
jsonnet-fmt:
	@for file in $(shell git ls-files | grep -e '\.jsonnet' -e '\.libsonnet'); do jsonnetfmt -i "$${file}"; done

.PHONY: check-jsonnet-fmt
check-jsonnet-fmt:
	@$(MAKE) jsonnet-fmt
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files | grep -e '\.jsonnet' -e '\.libsonnet') && \
		echo "Formatting up-to-date!"

GITLAB_CHANGELOG_VERSION ?= latest
GITLAB_CHANGELOG = .tmp/gitlab-changelog-$(GITLAB_CHANGELOG_VERSION)

.PHONY: generate-changelog
generate-changelog: export CHANGELOG_RELEASE ?= $(VERSION)
generate-changelog: $(GITLAB_CHANGELOG)
	# Generating new changelog entries
	@$(GITLAB_CHANGELOG) \
			-project-id 30899460 \
			-release $(CHANGELOG_RELEASE) \
			-config-file .gitlab/changelog.yml \
			-changelog-file CHANGELOG.md

$(GITLAB_CHANGELOG): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(GITLAB_CHANGELOG): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/gitlab-changelog/$(GITLAB_CHANGELOG_VERSION)/gitlab-changelog-$(OS_TYPE)-amd64"
$(GITLAB_CHANGELOG):
	# Installing $(DOWNLOAD_URL) as $(GITLAB_CHANGELOG)
	@mkdir -p $(shell dirname $(GITLAB_CHANGELOG))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(GITLAB_CHANGELOG)"
	@chmod +x "$(GITLAB_CHANGELOG)"
