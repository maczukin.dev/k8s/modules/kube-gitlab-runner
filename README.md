# K8S GitLab Runner module in JSONNET

[JSONNET](https://jsonnet.org) based module for preparing [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
deployment on Kubernetes.

Usable for GitLab Runner deployments managed by [Grafana Tanka](https://tanka.dev/).
You can think of it like a replacement for Helm Packages. But without packaging.
And with many options to append the generated manifests before they are applied to your
Kubernetes cluster.

## Usage

Download the `jsonnet/kube-gitlab-runner` content to your JSONNET project.
If you're using [jsonnet-bundler](https://github.com/jsonnet-bundler/jsonnet-bundler)
you can add this to your `jsonnetfile.json` file in the `dependencies` array:

```json
{
  "source": {
    "git": {
      "remote": "https://gitlab.com/maczukin.dev/k8s/modules/kube-gitlab-runner.git",
      "subdir": "jsonnet/kube-gitlab-runner"
    }
  },
  "version": "master"
}
```

and run `jb install`.

Having the library available, you can import it in your project. In the file, where you
want to use the GitLab Runner kubernetes manifests, add:

```jsonnet
local kubeGitLabRunner = import 'gitlab.com/maczukin.dev/k8s/modules/kube-gitlab-runner/jsonnet/kube-gitlab-runner';
```

Having the library imported, you can prepare the manifests for GitLab Runner deployment:

```jsonnet
local runner = kubeGitLabRunner({
  namespace: 'default',
  releaseName: 'simple',
  tokens+: {
    registration: '__REDACTED__',
  },
  registration+: {
    URL: 'https://gitlab.com',
  },
});
```

This will generate the object representing the manifests:

```jsonnet
{
    configmap: {...},
    deployment: {...},
    namespace: {...},
    rbac: {},
    secrets: {
      tokens: {...},
    }
}
```

Tools like _Grafana Tanka_ are able to get such object and transform it to
valid Kubernetes YAML manifests that can be next applied to the cluster.

While the Kube GitLab Runner project was created with _Grafana Tanka_ in mind,
there is nothing in the project itself that depends on it. If you want to use
these generated Kubernetes manifests for some other purpose or inside of your
own project that manages different configurations and deploy them in its own way
- go for it!

Check the [examples](./examples) directory for some more examples for how this
project could be used.

### Configuration and adjustments

There are two ways how the manifests can be adjusted.

#### Configuration options object

First is the configuration provided by the library. Several configuration options
are available out-of-a-box, and they can adjust what the `kubeGitLabRunner()` function
call used in the example above would produce.

To find all the available configuration options, please look into the
[`jsonnet/kube-gitlab-runner/main.libsonnet`](./jsonnet/kube-gitlab-runner/main.libsonnet)
file. The `defaultParams` object contains the options and comments describing
their purpose.

Important things to remember:

1. Some options are required. All such fields will have no default value
   and instead the `error 'must provide ...'` assigned to them. At the moment
   of writing this documentation, these are the `namespace`, `releaseName` 
   and `registration.URL` options.

2. A special case is the `tokens` object. It contains two keys that don't have any assigned
   value nor an error. Exactly one - and only one! - of them must get defined. So you need
   to specify either the `tokens.auth` or `tokens.registration` entry. Using both or not using
   any of them will generate an error.

The configuration options object works as any other object in JSONNET. Options passed
through the `kubeGitLabRunner(params)` function `params` argument are added to the `defaultParams`
object. Therefore, you can use all the JSONNET language features to overwrite or append the
default settings.

#### Adjusting the generated object

The second way of configuration is to operate on the generated Kubernetes manifest object.

When `kubeGitLabRunner()` will get executed, it will generate the output object
representing the Kubernetes manifests. This object is again a normal JSONNET object.
So you can update and/or append anything you want.

Depending on your needs and experience, you can adjust the fields manually, use some
[Kubernetes JSOONET library](https://github.com/jsonnet-libs/k8s-libsonnet.git) or
add updates in any other way.

Tools like _Grafana Tanka_ expect to get a final object, generated from the JSONNET file.
Howe it's generated and on what leves it's updated is not important. The final object will
be taken, translated to the Kubernetes YAML manifests and applied to the cluster.

## License

MIT

## Author

Tomasz Maczukin, 2021
