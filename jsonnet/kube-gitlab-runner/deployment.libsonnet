local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local container = k.core.v1.container;
local containerPort = k.core.v1.containerPort;
local deployment = k.apps.v1.deployment;
local probe = k.core.v1.probe;
local volume = k.core.v1.volume;
local volumeMount = k.core.v1.volumeMount;
local volumeProjection = k.core.v1.volumeProjection;

local newProbe =
  function(
    command,
    initialDelaySeconds,
    timeoutSeconds=1,
    periodSeconds=10,
    successThreshold=1,
    failureThreshold=3,
  )
    assert std.isArray(command) : '`command` must be an array';
    assert initialDelaySeconds > 0 : '`initialDelaySeconds` must be greater than 0';

    probe.exec.withCommand(command) +
    probe.withInitialDelaySeconds(initialDelaySeconds) +
    probe.withTimeoutSeconds(timeoutSeconds) +
    probe.withPeriodSeconds(periodSeconds) +
    probe.withSuccessThreshold(successThreshold) +
    probe.withFailureThreshold(failureThreshold);

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  image: error 'must provide `image`',
  labels: error 'must provide `labels`',
  rbac: {
    serviceAccountName: error 'must provide `rbac.serviceAccountName`',
  },
  metrics: {
    enabled: error 'must provide `metrics.enabled`',
    port: error 'must provide `metrics.port`',
  },
  deployment: {
    replicas: error 'must provide `deployment.replicas`',
    revisionHistoryLimit: error 'must provide `deployment.revisionHistoryLimit`',
    terminationGracePeriodSeconds: error 'must provide `deployment.terminationGracePeriodSeconds`',
    imagePullPolicy: error 'must provide `deployment.imagePullPolicy`',
    securityContext: {
      runAsUser: error 'must provide `deployment.securityContext.runAsUser`',
      fsGroup: error 'must provide `deployment.securityContext.fsGroup`',
    },
    resources: error 'must provide `deployment.resources`',
    envVars: error 'must provide `deployment.envVars`',
  },
  tokens: {
    secretName: error 'must provide `tokens.secretName`',
  },
  registration: {
    URL: error 'must provide `registration.URL`',
    requestConcurrency: error 'must provide `registration.requestConcurrency`',
    executor: error 'must provide `registration.executor`',
    tagList: error 'must provide `registration.tagList`',
    runUntagged: error 'must provide `registration.runUntagged`',
    locked: error 'must provide `registration.locked`',
    paused: error 'must provide `registration.paused`',
    maximumTimeout: error 'must provide `registration.maximumTimeout`',
    accessLevel: error 'must provide `registration.accessLevel`',
    leaveRunner: error 'must provide `registration.leaveRunner`',
    unregisterAtShutdown: error 'must provide `registration.unregisterAtShutdown`',
    maxRegisterAttempts: error 'must provide `registration.maxRegisterAttempts`',
  },
  certsSecretName: error 'must provide `certsSecretName`',
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';
  assert std.isArray(config.deployment.envVars) : '`deployment.envVars` must be an array';
  assert config.registration.URL != '' : '`registration.URL` must be provided';
  assert std.isArray(config.registration.tagList) : '`registration.tagList` must be an array';
  assert config.registration.maxRegisterAttempts > 0 : '`registration.maxRegisterAttempts` must be greater than 0';

  local registrationEnvVars =
    [
      // GitLab Runner registration parameters
      { name: 'CI_SERVER_URL', value: config.registration.URL },
      { name: 'RUNNER_REQUEST_CONCURRENCY', value: '%d' % config.registration.requestConcurrency },
      { name: 'RUNNER_EXECUTOR', value: config.registration.executor },
      { name: 'RUNNER_TAG_LIST', value: std.join(',', config.registration.tagList) },
      { name: 'REGISTER_RUN_UNTAGGED', value: '%s' % config.registration.runUntagged },
      { name: 'REGISTER_LOCKED', value: '%s' % config.registration.locked },
      { name: 'REGISTER_PAUSED', value: '%s' % config.registration.paused },
      { name: 'REGISTER_MAXIMUM_TIMEOUT', value: '%d' % config.registration.maximumTimeout },
      { name: 'REGISTER_ACCESS_LEVEL', value: config.registration.accessLevel },
      { name: 'REGISTER_LEAVE_RUNNER', value: '%s' % config.registration.leaveRunner },
      // Set to unlimited as we support only single `[[runners]]` through this deployment and the concurrency
      // will be controlled through the global `concurrent` setting.
      { name: 'RUNNER_LIMIT', value: '0' },

      // Configmap registration script parameters
      { name: 'MAX_REGISTER_ATTEMPTS', value: '%d' % config.registration.maxRegisterAttempts },
    ];

  local metricsAnnotations =
    if config.metrics.enabled then
      {
        'prometheus.io/scrape': 'true',
        'prometheus.io/port': '9252',
      }
    else
      {};

  local metricsPorts =
    if config.metrics.enabled then
      [
        containerPort.newNamed(config.metrics.port, 'metrics'),
      ]
    else
      [];

  local resources =
    {} +
    (
      if std.objectHas(config.deployment.resources, 'limits') then
        container.resources.withLimits(config.deployment.resources.limits)
      else
        {}
    ) +
    (
      if std.objectHas(config.deployment.resources, 'requests') then
        container.resources.withLimits(config.deployment.resources.requests)
      else
        {}
    );

  local volumeDef = {
    configMap: {
      name: 'configmap',
      path: '/configmaps',
    },
    etcGitLabRunner: {
      name: 'etc-gitlab-runner',
      path: '/home/gitlab-runner/.gitlab-runner',
    },
    customCerts: {
      name: 'custom-certs',
      path: '/home/gitlab-runner/.gitlab-runner/certs/',
    },
    runnerSecrets: {
      name: 'runner-secrets',
      path: '/runner-secrets/',
    },
    initSecrets: {
      name: 'init-secrets',
      path: '/init-secrets/',
    },
  };

  local volumes = [
    // Configmap
    volume.withName(volumeDef.configMap.name) +
    volume.configMap.withName(config.fullName),

    // Runtime storage for GitLab Runner configuration directory
    volume.withName(volumeDef.etcGitLabRunner.name) +
    volume.emptyDir.withMedium('Memory'),

    // Runtime storage for GitLab Runner secrets
    volume.withName(volumeDef.runnerSecrets.name) +
    volume.emptyDir.withMedium('Memory'),

    // Secrets
    volume.withName(volumeDef.initSecrets.name) +
    volume.projected.withSources([
      volumeProjection.secret.withName(config.tokens.secretName),
    ]),

    // secrets containing custom certificates to configure GitLab Runner
    if config.certsSecretName != '' then
      volume.withName(volumeDef.customCerts.name) +
      volume.secret.withSecretName(config.certsSecretName)
    else
      {},
  ];

  local commonVolumeMounts = [
    volumeMount.new(volumeDef.configMap.name, volumeDef.configMap.path, readOnly=false),
    volumeMount.new(volumeDef.etcGitLabRunner.name, volumeDef.etcGitLabRunner.path, readOnly=false),
    volumeMount.new(volumeDef.runnerSecrets.name, volumeDef.runnerSecrets.path, readOnly=false),

    if config.certsSecretName != '' then
      volumeMount.new(volumeDef.customCerts.name, volumeDef.customCerts.path, readOnly=true),
  ];

  local initContainers = [
    container.new('configure', config.image) +
    container.withImagePullPolicy(config.deployment.imagePullPolicy) +
    container.securityContext.withAllowPrivilegeEscalation(false) +
    container.withCommand(['sh', '/configmaps/configure.sh']) +
    container.withEnv(std.prune(config.deployment.envVars)) +
    container.withVolumeMounts(std.prune(
      commonVolumeMounts +
      [
        volumeMount.new(volumeDef.initSecrets.name, volumeDef.initSecrets.path, readOnly=true),
      ]
    )) +
    resources,
  ];

  local containers = [
    container.new('runner-manager', config.image) +
    container.withImagePullPolicy(config.deployment.imagePullPolicy) +
    container.securityContext.withAllowPrivilegeEscalation(false) +
    container.withCommand(['/usr/bin/dumb-init', '--', '/bin/bash', '/configmaps/entrypoint.sh']) +
    container.withPorts(std.prune(metricsPorts)) +
    container.withEnv(std.prune(
      config.deployment.envVars +
      registrationEnvVars,
    )) +
    container.withVolumeMounts(std.prune(commonVolumeMounts)) +
    resources +
    {
      livenessProbe: newProbe(['/bin/bash', '/configmaps/check-live.sh'], 60),
      readinessProbe: newProbe(['/usr/bin/pgrep', 'gitlab.*runner'], 10),
    } +
    (
      if config.registration.unregisterAtShutdown then
        container.lifecycle.preStop.exec.withCommand(['/entrypoint', 'unregister', '--all-runners'])
      else
        {}
    ),
  ];

  deployment.new(
    config.fullName,
    replicas=config.deployment.replicas,
    containers=containers,
    podLabels=config.labels,
  ) +
  deployment.metadata.withNamespace(config.namespace) +
  deployment.spec.withRevisionHistoryLimit(config.deployment.revisionHistoryLimit) +
  deployment.spec.template.metadata.withNamespace(config.namespace) +
  deployment.spec.template.spec.withServiceAccountName(config.rbac.serviceAccountName) +
  deployment.spec.template.spec.withTerminationGracePeriodSeconds(config.deployment.terminationGracePeriodSeconds) +
  deployment.spec.template.spec.securityContext.withRunAsUser(config.deployment.securityContext.runAsUser) +
  deployment.spec.template.spec.securityContext.withFsGroup(config.deployment.securityContext.fsGroup) +
  deployment.spec.template.spec.withInitContainers(initContainers) +
  deployment.spec.template.spec.withVolumes(std.prune(volumes)) +
  if std.length(std.prune(metricsAnnotations)) > 0 then
    deployment.spec.template.metadata.withAnnotations(std.prune(metricsAnnotations))
  else
    {}
