local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local namespace = k.core.v1.namespace;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  labels: error 'must provide `labels`',
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  namespace.new(config.namespace) +
  namespace.metadata.withLabels(config.labels)
