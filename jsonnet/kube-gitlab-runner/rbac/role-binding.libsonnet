local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local roleBinding = k.rbac.v1.roleBinding;
local clusterRoleBinding = k.rbac.v1.clusterRoleBinding;
local subject = k.rbac.v1.subject;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  rbac: {
    serviceAccountName: error 'must provide `rbac.serviceAccountName`',
    clusterWideAccess: error 'must provide `rbac.clusterWideAccess`',
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  local rb = if config.rbac.clusterWideAccess then clusterRoleBinding else roleBinding;

  rb.new(config.fullName) +
  rb.metadata.withLabels(config.labels) +
  rb.roleRef.withApiGroup('rbac.authorization.k8s.io') +
  rb.roleRef.withName(config.fullName) +
  rb.roleRef.withKind(
    if config.rbac.clusterWideAccess then 'ClusterRole' else 'Role'
  ) +
  rb.withSubjects(
    subject.withKind('ServiceAccount') +
    subject.withName(config.rbac.serviceAccountName) +
    if !config.rbac.clusterWideAccess then
      subject.withNamespace(config.namespace)
    else
      {}
  ) +
  if !config.rbac.clusterWideAccess then
    rb.metadata.withNamespace(config.namespace)
  else
    {}
