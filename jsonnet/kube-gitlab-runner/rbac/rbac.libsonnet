local roleBinding = import './role-binding.libsonnet';
local role = import './role.libsonnet';
local serviceAccount = import './service-account.libsonnet';

local defaultParams = {
  rbac: {
    create: error 'must provide `rbac.create`',
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  if config.rbac.create then
    {
      serviceAccount: serviceAccount(params),
      role: role(params),
      roleBinding: roleBinding(params),
    }
  else
    {}
