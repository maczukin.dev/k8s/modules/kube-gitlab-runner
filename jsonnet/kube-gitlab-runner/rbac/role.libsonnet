local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local role = k.rbac.v1.role;
local clusterRole = k.rbac.v1.clusterRole;
local policyRule = k.rbac.v1.policyRule;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  rbac: {
    clusterWideAccess: error 'must provide `rbac.clusterWideAccess`',
    rules: error 'must provide `rbac.rules`',
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  local r = if config.rbac.clusterWideAccess then clusterRole else role;

  r.new(config.fullName) +
  r.metadata.withLabels(config.labels) +
  r.withRules(config.rbac.rules) +
  if !config.rbac.clusterWideAccess then
    r.metadata.withNamespace(config.namespace)
  else
    {}
