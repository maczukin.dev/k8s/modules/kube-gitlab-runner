local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local serviceAccount = k.core.v1.serviceAccount;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  rbac: {
    serviceAccountName: error 'must provide `rbac.serviceAccountName`',
    clusterWideAccess: error 'must provide `rbac.clusterWideAccess`',
    serviceAccountAnnotations: [],
    imagePullSecrets: [],
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  serviceAccount.new(config.rbac.serviceAccountName) +
  serviceAccount.metadata.withLabels(config.labels) +
  serviceAccount.metadata.withAnnotations(config.rbac.serviceAccountAnnotations) +
  serviceAccount.withImagePullSecrets(config.rbac.imagePullSecrets) +
  if !config.rbac.clusterWideAccess then
    serviceAccount.metadata.withNamespace(config.namespace)
  else
    {}
