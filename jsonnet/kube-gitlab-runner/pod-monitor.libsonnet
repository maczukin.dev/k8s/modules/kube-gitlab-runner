local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  metrics: {
    enabled: error 'must provide `metrics.enabled`',
    podMonitor: {
      podTargetLabels: error 'must provide `metrics.podMonitor.podTargetLabels`',
    },
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';
  assert std.isArray(config.metrics.podMonitor.podTargetLabels) : '`metrics.podMonitor.podTargetLabels` must be an array';

  if config.metrics.enabled then
    {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'PodMonitor',
      metadata: {
        name: '%s-metrics' % config.fullName,
        namespace: config.namespace,
        labels: config.labels,
      },
      spec: {
        selector: {
          matchLabels:
            config.labels
            {
              name: config.fullName,
            },
        },
        podMetricsEndpoints: [
          {
            port: 'metrics',
          },
        ],
        podTargetLabels: config.metrics.podMonitor.podTargetLabels,
      },
    }
  else
    {}
