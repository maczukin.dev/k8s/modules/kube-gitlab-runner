local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local configMap = k.core.v1.configMap;

// Keep the directories in sync with the volume definitions in deploynet.libsonnet
local configureScript = |||
  set -eo pipefail

  cp /init-secrets/* /runner-secrets
|||;

local entrypointScript = |||
  set -eo pipefail

  # Prepare initial configuration
  mkdir -p /home/gitlab-runner/.gitlab-runner/
  cp /configmaps/config.toml /home/gitlab-runner/.gitlab-runner/

  # Run registration
  if ! bash /configmaps/register-the-runner.sh; then
    exit 1
  fi

  # Run preEntrypointScript (if defined)
  if [[ -f /configmaps/pre-entrypoint-script.sh ]]; then
    if ! bash /configmaps/pre-entrypoint-script.sh; then
      exit 1
    fi
  fi

  # Start the runner
  exec /entrypoint \
        run \
        --user=gitlab-runner \
        --working-directory=/home/gitlab-runner
|||;

local registerTheRunnerScript = |||
  for i in $(seq 1 "${MAX_REGISTER_ATTEMPTS}"); do
    echo "Registration attempt ${i} of ${MAX_REGISTER_ATTEMPTS}"

    if [[ -f /runner-secrets/runner-registration-token ]]; then
      export REGISTRATION_TOKEN=$(cat /runner-secrets/runner-registration-token)
    elif [[ -f /runner-secrets/runner-auth-token ]]; then
      export CI_SERVER_TOKEN=$(cat /runner-secrets/runner-auth-token)
    fi

    /entrypoint register \
      --config /home/gitlab-runner/.gitlab-runner/config.toml \
      --template-config /configmaps/config.template.toml \
      --non-interactive

    retval=$?

    if [[ ${retval} -eq 0 ]]; then
      break
    elif [[ ${i} -eq ${MAX_REGISTER_ATTEMPTS} ]]; then
      exit 1
    fi
  sleep 5

  done

  exit 0
|||;

local checkLiveScript = |||
  if /usr/bin/pgrep -f .*register-the-runner.sh; then
    exit 0
  elif /usr/bin/pgrep gitlab.*runner; then
    exit 0
  else
    exit 1
  fi
|||;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  metrics: {
    enabled: error 'must provide `metrics.enabled`',
    port: error 'must provide `metrics.port`',
  },
  preEntrypointScript: error 'must provide `preEntrypointScript`',
  configTOML: {
    concurrent: error 'must provide `configTOML.concurrent`',
    checkInterval: error 'must provide `configTOML.checkInterval`',
    logLevel: error 'must provide `configTOML.logLevel`',
    logFormat: error 'must provide `configTOML.logFormat`',
    sentryDSN: error 'must provide `configTOML.sentryDSN`',
  },
  configTemplateTOML: error 'must provide `configTemplateTOML`',
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';
  assert config.configTemplateTOML != '' : "`configTemplateTOML` can't be blank and must have at least one `[[runners]]` section";

  local configTOML =
    |||
      # Generated automatically by the deployment. Manual changes will be overriden!

      concurrent = %(concurrent)d
      check_interval = %(checkInterval)d
      log_level = "%(logLevel)s"
      log_format = "%(logFormat)s"
    ||| % {
      concurrent: config.configTOML.concurrent,
      checkInterval: config.configTOML.checkInterval,
      logLevel: config.configTOML.logLevel,
      logFormat: config.configTOML.logFormat,
    } +
    (
      if config.configTOML.sentryDSN != '' then
        'sentry_dsn = "%s"' % config.configTOML.sentryDSN
      else
        ''
    ) +
    (
      if config.metrics.enabled then
        'listen_address = ":%d"' % config.metrics.port
      else
        ''
    );

  local data =
    {
      'check-live.sh': checkLiveScript,
      'configure.sh': configureScript,
      'entrypoint.sh': entrypointScript,
      'register-the-runner.sh': registerTheRunnerScript,
      'config.toml': configTOML,
      'config.template.toml': config.configTemplateTOML,
    } +
    if config.preEntrypointScript != '' then
      { 'pre-entrypoint-script.sh': config.preEntrypointScript }
    else
      {};

  configMap.new(config.fullName, data) +
  configMap.metadata.withNamespace(config.namespace) +
  configMap.metadata.withLabels(config.labels)
