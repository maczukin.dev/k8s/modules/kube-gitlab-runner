local configmap = import './configmap.libsonnet';
local deployment = import './deployment.libsonnet';
local namespace = import './namespace.libsonnet';
local podMonitor = import './pod-monitor.libsonnet';
local rbac = import './rbac/rbac.libsonnet';
local secrets = import './secrets/secrets.libsonnet';

local VERSION = '0.1-beta';

local defaultParams = {
  local s = self,

  // Version of the kube-gitlab-runner
  // Hardcoded. Updates will be overwritten!
  __version: VERSION,

  // A namespace in which all of the resources are created
  namespace: error 'must provide `namespace`',

  // Name for the release deployed with this specification
  releaseName: error 'must provide `releaseName`',

  // Name of the deployment specification. Used in different places.
  // Do not change unless being sure what it will affect!
  name: 'gitlab-runner',

  // Version of GitLab Runner to use.
  runnerVersion: 'v14.4.0',

  // A helper field containing a name for different resources
  fullName: '%s-%s' % [s.releaseName, s.name],

  // Details of the container image to use for the Runner Manager deployment
  imageName: 'registry.gitlab.com/gitlab-org/gitlab-runner',
  imageFlavour: 'alpine',
  image: '%s:%s-%s' % [s.imageName, s.imageFlavour, s.runnerVersion],

  // Set of common labels for the Kubernetes manifests created by this deployment
  // Some of the labels are hardcoded and will override the custom values
  // in case of conflicts!
  labels: {},

  rbac+: {
    // Name of service account that will be attached to Runner Manager pod.
    serviceAccountName:
      if s.rbac.create then
        s.fullName
      else
        '',

    // Should the details of RBAC be created by Runner. If not, the service account pointed by `serviceAccountName`
    // must be created and properly configured by the user before deploying GitLab Runner.
    // All further `rbac` settings are ignored when `create` is left as `false`.
    create: false,

    // Should the role and role bindings be created at the cluster level?
    clusterWideAccess: false,

    // Additional annotations for the service account managed by GitLab Runner deployment
    serviceAccountAnnotations: {},

    // Image Pull Secrets maintained through service account
    imagePullSecrets: [],

    // Permission rules defined for the managed role
    rules: [
      {
        apiGroups: ['*'],
        resources: ['*'],
        verbs: ['*'],
      },
    ],
  },

  deployment+: {
    // Number of replicas of the deployment. Must be 1 when tokens.auth is defined
    // https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#replicas
    replicas: 1,

    // Limit of old ReplicaSets for the Deployment to be stored
    // https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#revision-history-limit
    revisionHistoryLimit: 10,

    // Time defined as termination grace period. Should be a longer value, because runner
    // will start the Graceful Shutdown procedure, which needs some time to first finish
    // all already running jobs. The shorter the time, the more jobs will get interrupted
    // in the middle.
    // ref: https://docs.gitlab.com/runner/commands/#signals
    terminationGracePeriodSeconds: 3600,

    // Pull policy to use when creating Runner Manager pod containers
    // ref: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
    imagePullPolicy: 'IfNotPresent',

    // Security context for the Runner Manager pod
    // ref: http://kubernetes.io/docs/user-guide/security-context/
    securityContext+: {
      runAsUser: 100,
      fsGroup: 65533,
    },

    // Resource requests and limits
    // ref: http://kubernetes.io/docs/user-guide/compute-resources/
    resources: {
      // limits: {},
      // requests: {},
    },

    // Map of additional env vars that should be passed to the Runner Manager pod containers
    envVars: [
      // { name: 'VARIABLE_NAME', value: 'variable value' },
    ],
  },

  tokens+: {
    // ONLY ONE OF THE TOKENS BELLOW CAN BE DEFINED!

    // Authentication token of the already registered runner. Will prevent registration and
    // just set the configuration details.
    // auth: '',

    // Registration token. Will register a new Runner and unregister it at the container shutdown.
    // registration: '',

    // Name of the secret that will be created to store the provided tokens
    secretName: '%s-tokens' % s.fullName,
  },

  // Details needed for registration
  registration+: {
    // GitLab server URL
    URL: error 'must provide `registration.URL`',

    // Maximum concurrency for job requests
    requestConcurrency: 1,

    // Which executor should be used
    executor: 'kubernetes',

    // An array containing the list of tags that should be attached to the registered runner
    tagList: [],

    // Should this runner handle untagged jobs
    runUntagged: true,

    // Should this runner be locked to the project where it's registered (usable only for project runners)
    locked: true,

    // Should this runner be initially paused on GitLab side
    paused: false,

    // Maximum timeout (in seconds) that the runner will allow the job to execute. Takes precedence over
    // the timeout defined at project or job level.
    // `0` means `no timeout`.
    maximumTimeout: 0,

    // Set `access_level` of the runner to `not_protected` or `ref_protected`.
    accessLevel: 'not_protected',

    // Should this runner be left without unregistering when the registration fails
    leaveRunner: false,

    // Should GitLab Runner unregister itself when exiting the container
    // Defaults to `true` when registration token is used
    unregisterAtShutdown: std.objectHas(s.tokens, 'registration'),

    // How many times to try to register the Runner before failing the container initialization
    maxRegisterAttempts: 30,
  },

  metrics+: {
    // Should Prometheus metrics exporter built into GitLab Runner be enabled.
    // When enabled, the
    // If set to `false` none of the service discovery method mentioned bellow will be configured.
    enabled: true,

    // TCP port on which the internal Prometheus metrics exporter will be listening
    port: 9252,

    // Additional customisation of PodMonitor resource created for Prometheus Opeartor's autodiscovery
    podMonitor: {
      // List of pod labels that will be set as metric labels for all metrics scraped from the target
      podTargetLabels: [],
    },
  },

  // Set the certsSecretName in order to pass custom certficates for GitLab Runner to use
  // Provide resource name for a Kubernetes Secret Object in the same namespace,
  // this is used to populate the /home/gitlab-runner/.gitlab-runner/certs/ directory
  // ref: https://docs.gitlab.com/runner/configuration/tls-self-signed.html#supported-options-for-self-signed-certificates
  certsSecretName: '',

  // A custom bash script that will be executed prior to the invocation
  // gitlab-runner process
  preEntrypointScript: '',

  // Global section of config.toml
  // ref: https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-global-section
  configTOML+: {
    concurrent: 10,
    checkInterval: 5,
    logLevel: 'info',
    logFormat: 'json',

    // disabled by default
    sentryDSN: '',
  },

  // The [[runners]] configuration template content
  // ref: https://docs.gitlab.com/runner/register/index.html#runners-configuration-template-file
  configTemplateTOML: |||
    [[runners]]
      [runners.kubernetes]
        namespace = "%(namespace)s"
  ||| % {
    namespace: s.namespace,
  },
};

local hardcodeCommonLabels = function(config)
  config {
    labels+: {
      'app.kubernetes.io/name': config.name,
      'app.kubernetes.io/version': config.runnerVersion,
      'app.kubernetes.io/component': 'runner-manager',
      'app.kubernetes.io/managed-by': 'kube-gitlab-runner_%s' % VERSION,
    },
  };

local hardcodeVERSION = function(config)
  config {
    __version: VERSION,
  };

function(params={})
  local config =
    hardcodeCommonLabels(
      hardcodeVERSION(
        defaultParams +
        params
      )
    );

  assert std.isObject(config) : '`config` must be an object';
  assert std.length(config.fullName) <= 40 : '`fullName` must be no longer than 40';
  assert !(std.objectHas(config.tokens, 'auth') && config.replicas > 1) : '`replicas` must be `1` when `tokens.auth` is specified';

  {
    namespace: namespace(config),
    rbac: rbac(config),
    secrets: secrets(config),
    configmap: configmap(config),
    deployment: deployment(config),
    podMonitor: podMonitor(config),
  }
