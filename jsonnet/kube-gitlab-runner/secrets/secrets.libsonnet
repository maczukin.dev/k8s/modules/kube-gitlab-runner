local tokens = import './tokens.libsonnet';

function(params={})
  {
    tokens: tokens(params),
  }
