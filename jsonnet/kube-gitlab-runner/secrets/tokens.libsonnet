local k = import 'github.com/jsonnet-libs/k8s-libsonnet/1.21/main.libsonnet';

local secret = k.core.v1.secret;

local defaultParams = {
  namespace: error 'must provide `namespace`',
  fullName: error 'must provide `fullName`',
  labels: error 'must provide `labels`',
  tokens: {
    secretName: error 'must provide `tokens.secretName`',
  },
};

function(params={})
  local config = defaultParams + params;

  assert std.isObject(config) : '`config` must be an object';

  assert (std.objectHas(config.tokens, 'auth') && config.tokens.auth != '') || (std.objectHas(config.tokens, 'registration') && config.tokens.registration != '') : 'either `tokens.auth` or `tokens.registration` must be defined';
  assert !(std.objectHas(config.tokens, 'auth') && std.objectHas(config.tokens, 'registration')) : 'only one of `tokens.auth` or `tokens.registration` can be specified';

  local data =
    {} +
    (
      if std.objectHas(config.tokens, 'registration') then
        { 'runner-registration-token': std.base64(config.tokens.registration) }
      else
        {}
    ) +
    (
      if std.objectHas(config.tokens, 'auth') then
        { 'runner-auth-token': std.base64(config.tokens.auth) }
      else
        {}
    );

  secret.new(config.tokens.secretName, data) +
  secret.metadata.withLabels(config.labels) +
  secret.metadata.withNamespace(config.namespace)
