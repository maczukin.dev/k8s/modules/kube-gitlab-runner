ALPINE_VERSION ?= 3.14
KUBECTL_VERSION ?= v1.21.3
# See more: https://tanka.dev/
TANKA_VERSION ?= v0.18.2
# See more: https://github.com/jsonnet-bundler/jsonnet-bundler/
JSONNET_BUNDLER_VERSION ?= 65eeb986a83d42e2f754053fbbf075fb65166aac

CI_IMAGE ?= kube-gitlab-runner/ci:dev
CI_IMAGE_LATEST ?= kube-gitlab-runner/ci:latest

.PHONY: pull-ci-image-latest
pull-ci-image-latest:
	# Pull $(CI_IMAGE_LATEST) image
ifneq ($(CI_REGISTRY),)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker pull $(CI_IMAGE_LATEST) || echo "$(CI_IMAGE_LATEST) is not available; will not use the layer cache"
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value; skipping image pulling
endif

.PHONY: build-ci-image
build-ci-image:
	# Build $(CI_IMAGE) image
	@docker build \
		--tag $(CI_IMAGE) \
		--cache-from $(CI_IMAGE_LATEST) \
		--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
		--build-arg KUBECTL_VERSION=$(KUBECTL_VERSION) \
		--build-arg TANKA_VERSION=$(TANKA_VERSION) \
		--build-arg JSONNET_BUNDLER_VERSION=$(JSONNET_BUNDLER_VERSION) \
		-f ./dockerfiles/ci/Dockerfile \
		./dockerfiles/ci/
	@docker tag $(CI_IMAGE) $(CI_IMAGE_LATEST)

.PHONY: publish-ci-image
publish-ci-image:
	# Publish $(CI_IMAGE) image
	# Publish $(CI_IMAGE_LATEST) image
ifneq ($(CI_REGISTRY),)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker push $(CI_IMAGE_LATEST)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value; skipping image publication
endif
