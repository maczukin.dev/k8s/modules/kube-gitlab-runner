local rules = import './lib/rules.libsonnet';

local warning = "# Generated automatically with 'make build-gitlab-ci-yaml' - do not hand edit!\n\n";
local global =
  {
    stages: [
      'prepare',
      'test',
    ],
    variables: {
      DOCKER_VERSION: '20.10.10',
    },
    default: {
      image: '${CI_IMAGE}',
    },
    workflow: {
      rules: [
        rules.conditions.ifTag,
        rules.conditions.ifDefaultBranch,
        rules.conditions.ifMergeRequest,
      ],
    },
  };

warning + std.manifestYamlDoc(
  global +
  (import 'ci_image.libsonnet') +
  (import 'test.libsonnet')
)
