{
  local s = self,

  conditions:: {
    ifDefaultBranch: {
      'if': '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH',
    },
    ifNonDefaultBranch: {
      'if': '$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH',
    },
    ifTag: {
      'if': '$CI_COMMIT_TAG',
    },
    ifMergeRequest: {
      'if': '$CI_PIPELINE_SOURCE == "merge_request_event"',
    },
  },

  changes:: {
    with(changes): { changes+: if std.isArray(changes) then changes else [changes] },
    withExamples(example): s.changes.with([
      'examples/%s/*' % example,
      '.gitlab/ci/examples/%s.libsonnet' % example,
    ]),
  },

  when:: {
    onSuccess: { when: 'on_success' },
    manual: { when: 'manual' },
  },

  default:: [
    s.conditions.ifMergeRequest,
    s.conditions.ifDefaultBranch,
    s.conditions.ifTag,
  ],
}
