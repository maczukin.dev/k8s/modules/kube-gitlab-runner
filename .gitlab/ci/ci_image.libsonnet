local dind = import 'lib/dind.libsonnet';
local job = import 'lib/job.libsonnet';
local rules = import 'lib/rules.libsonnet';

{
  variables+: {
    CI_IMAGE: '${CI_REGISTRY_IMAGE}/ci:20211030.1',
    CI_IMAGE_LATEST: '${CI_REGISTRY_IMAGE}/ci:latest',
  },
  'prepare ci image':
    job.withStage('prepare') +
    job.withScript([
      'apk add --no-cache make',
      'make pull-ci-image-latest',
      'make build-ci-image',
      'make publish-ci-image',
    ]) +
    job.withRules([
      rules.conditions.ifMergeRequest +
      rules.changes.with([
        '.gitlab/ci/ci_image.libsonnet',
        'Makefile.ci-image.mk',
        'dockerfiles/ci/*',
      ]),
    ]) +
    dind,
}
