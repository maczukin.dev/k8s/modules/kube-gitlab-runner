local job = import 'lib/job.libsonnet';
local rules = import 'lib/rules.libsonnet';

{
  'jsonnet fmt':
    job.withStage('test') +
    job.withScript([
      'make check-jsonnet-fmt',
    ]),

  'tanka fmt':
    job.withStage('test') +
    job.withScript([
      'make check-tk-fmt',
    ]),

  'gitlab-ci.yml selftest':
    job.withStage('test') +
    job.withScript([
      'make check-gitlab-ci-yml',
    ]),
}
