.PHONY: tk-fmt
tk-fmt:
	@tk fmt examples/

.PHONY: check-tk-fmt
check-tk-fmt:
	@$(MAKE) tk-fmt
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files | grep -e '\.jsonnet' -e '\.libsonnet') && \
		echo "Formatting up-to-date!"

RUNNER_REGISTRATION_TOKEN_FILE ?= .registrationToken.secret

registrationToken := unknown
ifneq (,$(wildcard $(RUNNER_REGISTRATION_TOKEN_FILE)))
registrationToken := $(shell cat $(RUNNER_REGISTRATION_TOKEN_FILE))
endif

tkCommonFlags = --tla-code "secrets={registrationToken: '$(registrationToken)'}"

TANKA_ENV ?= simple

.PHONY: tanka-manifests-eval
tanka-manifests-eval:
	@tk eval examples/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-show
tanka-manifests-show:
	@tk show examples/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-export
tanka-manifests-export:
	@rm -rf build/
	@tk export build/ examples/$(TANKA_ENV) \
		--format '{{if .metadata.namespace}}{{.metadata.namespace}}.{{end}}{{.apiVersion}}.{{.kind}}.{{.metadata.name}}' \
		$(tkCommonFlags)

.PHONY: tanka-manifests-diff
tanka-manifests-diff:
	@tk diff --with-prune examples/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-apply
tanka-manifests-apply:
	@tk apply examples/$(TANKA_ENV) $(tkCommonFlags)
	@tk prune examples/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-delete
tanka-manifests-delete:
	@tk delete examples/$(TANKA_ENV) $(tkCommonFlags)
